﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace wAppSrvr.wasp
{
	class WaspClnt
	{
		// internal properties
		public Socket Sckt;
		public bool Connected
		{
			get
			{
				return SendData(new byte[0]);
			}
		}
		private byte[] buff;
		private bool pktGot, pktMode;
		private int wrtOff, pktSize;
		private const int BuffSize = 0x10000;

		// Connect to a client
		public static WaspClnt Connect(IPEndPoint ep)
		{
			bool success = false;
			Socket skt = new Socket(ep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			try
			{ skt.Connect(ep); success = true; }
			catch (SocketException) { }
			WaspClnt clnt = (success) ? new WaspClnt(skt) : null;
			return clnt;
		}

		// Client Creator
		public WaspClnt(Socket skt, bool pktmode)
		{
			Sckt = skt;
			buff = new byte[BuffSize];
			pktGot = false;
			wrtOff = 0;
			pktSize = 0;
			pktMode = pktmode;
		}
		public WaspClnt(Socket skt) : this(skt, true)
		{
		}

		// Recieve data
		public int RecieveData(byte[] buff, int off)
		{
			// get available data
			int rcvd = Sckt.Available;
			// if something is available, then get it
			if (rcvd > 0)
			{
				try
				{ rcvd = Sckt.Receive(buff, off, Sckt.Available, SocketFlags.None); }
				catch (SocketException) { }
			}
			return rcvd;
		}

		// Send data
		public bool SendData(byte[] data)
		{
			int sent = 0;
			bool success = false;
			try
			{ sent = Sckt.Send(data); success = true; }
			catch (SocketException) { }
			success = success && (sent == data.Length);
			return success;
		}

		// Packet Reciever
		public WaspPkt RecievePacket()
		{
			WaspPkt pkt = null;
			if (!pktMode)
			{
				if (Sckt.Available > 0)
				{
					byte[] data = new byte[Sckt.Available];
					RecieveData(data, 0);
					pkt = new WaspPkt(this, data);
				}
				return pkt;
			}
			int rcvd = RecieveData(buff, wrtOff);
			wrtOff += rcvd;
			if (!pktGot)
			{
				int i;
				for (i = 0; i < wrtOff; i++)
					if (buff[i] == WaspPkt.Begin) break;
				pktGot = (i < wrtOff);
				if (pktGot && i > 0) Array.Copy(buff, i, buff, 0, wrtOff - i);
				wrtOff -= i;
				pktSize = 0;
			}
			if(pktGot)
			{
				if (pktSize == 0 && wrtOff >= 3) pktSize = buff[1] | (buff[2] << 8);
				if (pktSize > 0 && wrtOff >= pktSize)
				{
					if (buff[pktSize - 1] == WaspPkt.End)
					{
						byte[] data = new byte[pktSize];
						Array.Copy(buff, data, pktSize);
						pkt = new WaspPkt(this, data);
					}
					wrtOff -= pktSize;
					Array.Copy(buff, pktSize, buff, 0, wrtOff);
					pktGot = false;
				}
			}
			return pkt;
		}

		// Client Close
		public void Close()
		{
			try
			{ Sckt.Disconnect(false); }
			catch(SocketException) {}
			try
			{ Sckt.Close(); }
			finally
			{
				Sckt = null;
				buff = null;
			}
		}
	}
}
