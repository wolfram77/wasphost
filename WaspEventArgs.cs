﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wAppSrvr.wasp
{
	public enum WaspEvent
	{
		ClientAcptd,
		ClientRjctd,
		ClientAdded,
		ClientAddFailed,
		ClientRmvd,
		PktRcvd,
		PktRcvFailed,
		PktSent,
		PktSndFailed
	}

	public class WaspEventArgs : EventArgs
	{
		public WaspEvent Event;
		public object Data;

		public WaspEventArgs(WaspEvent evnt, object data)
		{
			Event = evnt;
			Data = data;
		}
	}

	public delegate void WaspEventFn(object sender, WaspEventArgs e);
}
