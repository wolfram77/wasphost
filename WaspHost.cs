﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

// As of 2/11/2013 WaspHost supports only TCP (internally)
namespace wAppSrvr.wasp
{
	class WaspHost
	{
		// Internal properties
		private Socket server;
		private ProtocolType protocol;
		private WaspList<WaspClnt> client;
		private WaspList<IPEndPoint> clientAdd;
		private WaspList<WaspClnt> clientRmv;
		private WaspList<WaspPkt> rcvPkt, sndPkt;
		private Thread thClientAcptr, thClientChkr, thClientAddr, thClientRmvr, thRcvPkt, thSndPkt;
		private event WaspEventFn fnClientAcptd, fnClientRjctd, fnClientAdded, fnClientAddFailed;
		private event WaspEventFn fnClientRmvd, fnPktRcvd, fnPktRcvFailed, fnPktSent, fnPktSndFailed;
		private int rcvPkts, sndPkts;
		public ProtocolType Protocol
		{
			get
			{ return protocol; }
		}
		public int Clients
		{
			get { return client.Count; }
		}
		public int RcvPkts
		{
			get
			{ return rcvPkts; }
		}
		public int SndPkts
		{
			get
			{ return sndPkts; }
		}
		public int PendingRcvPkts
		{
			get { return rcvPkt.Count; }
		}
		public int PendingSndPkts
		{
			get { return sndPkt.Count; }
		}
		private static int hosts = 0;
		public static int Hosts
		{
			get
			{ return hosts; }
		}
		private const int maxPendingConns = 16;
		private const int noPktSleepTime = 16;
		private static TimeSpan clientCheckTime = new TimeSpan(0, 2, 0);
		private static TimeSpan clientRecheckTime = new TimeSpan(0, 10, 0);

		
		// Summary:
		//	Creates a WASP Host, which can act as a server or a client, and 
		//	support UDP or TCP protocol communication.
		// 
		// Parameters:
		//	addr:		IP address where the host must be created (null for client).
		//	port:		Port number of the host (NA for client).
		//	prtcl:		protocol to use (TCP / UDP only).
		// 
		public WaspHost(IPEndPoint ep, ProtocolType prtcl)
		{
			// create a server socket, if required
			if (ep == null) server = null;
			else
			{
				switch (prtcl)
				{
					case ProtocolType.Tcp:
						server = new Socket(ep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
						break;
					//case ProtocolType.Udp:
					//	server = new Socket(ep.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
					//	break;
					default:
						throw new NotSupportedException();
				}
				server.Bind(ep);
				if(prtcl == ProtocolType.Tcp) server.Listen(maxPendingConns);
			}
			protocol = prtcl;
			// initialize lists for maintaining information
			if (prtcl == ProtocolType.Tcp)
			{
				client = new WaspList<WaspClnt>(true);
				clientAdd = new WaspList<IPEndPoint>(true);
				clientRmv = new WaspList<WaspClnt>(true);
			}
			rcvPkt = new WaspList<WaspPkt>();
			sndPkt = new WaspList<WaspPkt>(true);
			// initialize counts
			rcvPkts = 0;
			sndPkts = 0;
			// launch threads for performing independent activities
			if (ep != null && prtcl == ProtocolType.Tcp)
			{
				thClientAcptr = new Thread(new ThreadStart(thread_ClientAcptr));
				thClientAcptr.Name = "WaspHost" + hosts + "_ClientAcptr";
				thClientAcptr.IsBackground = true;
				thClientAcptr.Start();
			}
			if (prtcl == ProtocolType.Tcp)
			{
				thClientChkr = new Thread(new ThreadStart(thread_ClientChkr));
				thClientChkr.Name = "WaspHost" + hosts + "_ClientChkr";
				thClientChkr.IsBackground = true;
				thClientChkr.Start();
			}
			if (prtcl == ProtocolType.Tcp)
			{
				thClientAddr = new Thread(new ThreadStart(thread_ClientAddr));
				thClientAddr.Name = "WaspHost" + hosts + "_ClientAddr";
				thClientAddr.IsBackground = true;
				thClientAddr.Start();
			}
			if (prtcl == ProtocolType.Tcp)
			{
				thClientRmvr = new Thread(new ThreadStart(thread_ClientRmvr));
				thClientRmvr.Name = "WaspHost" + hosts + "_ClientRmvr";
				thClientRmvr.IsBackground = true;
				thClientRmvr.Start();
			}
			thRcvPkt = new Thread(new ThreadStart(thread_RcvPkt));
			thRcvPkt.Name = "WaspSrvr" + hosts + "_RcvPkt";
			thRcvPkt.IsBackground = true;
			thRcvPkt.Start();
			thSndPkt = new Thread(new ThreadStart(thread_SndPkt));
			thSndPkt.Name = "WaspSrvr" + hosts + "_SndPkt";
			thSndPkt.IsBackground = true;
			thSndPkt.Start();
			hosts++;
		}

		// Thread: Client Accepter (TCP server mode only)
		private void thread_ClientAcptr()
		{
			// run only in server mode
			if (server == null || protocol == ProtocolType.Udp) return;
			while (true)
			{
				// accept any new incoming connection
				WaspClnt clnt = new WaspClnt(server.Accept());
				client.AddP(clnt);
				if (fnClientAcptd != null) fnClientAcptd(this, new WaspEventArgs(WaspEvent.ClientAcptd, clnt));
			}
		}

		// Thread: Client Checker (TCP mode only)
		private void thread_ClientChkr()
		{
			int i = 0;
			// run in TCP protocol only
			if (protocol != ProtocolType.Tcp) return;
			while (true)
			{
				// wait for some time before checking (each connection)
				Thread.Sleep(clientCheckTime);
				WaspClnt clnt = client.NextElementP(ref i);
				// if client found, and is not connected, then
				if (clnt != null && !clnt.Connected)
				{
					// remove the client from client list
					if (fnClientRjctd != null) fnClientRjctd(this, new WaspEventArgs(WaspEvent.ClientRjctd, clnt));
					clnt.Close();
					client.RemoveP(clnt);
				}
				// wait for recheck time once every client list traversal
				if (i == 0) Thread.Sleep(clientRecheckTime);
			}
		}

		// Thread: Client Adder (TCP mode only)
		private void thread_ClientAddr()
		{
			// runs only with TCP protocol
			if (protocol != ProtocolType.Tcp) return;
			while (true)
			{
				IPEndPoint ep = clientAdd.PopP();
				// connect to endpoint, if available
				if (ep != null)
				{
					WaspClnt clnt = WaspClnt.Connect(ep);
					// on success, add it to client list, and call success event
					if (clnt != null)
					{
						client.AddP(clnt);
						if (fnClientAdded != null) fnClientAdded(this, new WaspEventArgs(WaspEvent.ClientAdded, ep));
					}
					// of failure, call failure event
					else if (fnClientAddFailed != null) fnClientAddFailed(this, new WaspEventArgs(WaspEvent.ClientAddFailed, ep));
				}
			}
		}

		// Thread: Client Remover (TCP protocol only)
		private void thread_ClientRmvr()
		{
			WaspClnt clnt;
			// runs only with TCP protocol
			if (protocol != ProtocolType.Tcp) return;
			while (true)
			{
				clnt = clientRmv.PopP();
				// connect to endpoint, if available
				if (clnt != null)
				{
					// remove it to client list, and call remove event
					client.RemoveP(clnt);
					if (fnClientRmvd != null) fnClientRmvd(this, new WaspEventArgs(WaspEvent.ClientRmvd, clnt));
				}
			}
		}

		// Thread: Recieve Packet
		private void thread_RcvPkt()
		{
			int i = 0, emptypkt = 0;
			while (true)
			{
				if (emptypkt > client.Count)
				{
					emptypkt = 0;
					Thread.Sleep(noPktSleepTime);
				}
				WaspClnt clnt = client.NextElementP(ref i);
				if (clnt != null)
				{
					WaspPkt pkt = clnt.RecievePacket();
					if (pkt != null)
					{
						emptypkt = 0;
						rcvPkt.AddP(pkt);
						lock (rcvPkt.Lock)
						{ rcvPkts++; }
						continue;
					}
				}
				emptypkt++;
			}
		}

		// Thread: Send Packet
		private void thread_SndPkt()
		{
			while (true)
			{
				WaspPkt pkt = sndPkt.PopP();
				// send packet, if available
				if (pkt != null)
				{
					bool success = pkt.Send();
					if (success)
					{
						lock (sndPkt.Lock)
						{ sndPkts++; }
						if (fnPktSent != null) fnPktSent(this, new WaspEventArgs(WaspEvent.PktSent, pkt));
					}
					else if (fnPktSndFailed != null) fnPktSndFailed(this, new WaspEventArgs(WaspEvent.PktSndFailed, pkt));
				}
			}
		}

		// Get Client
		public WaspClnt GetClient(int indx)
		{
			return (protocol == ProtocolType.Tcp)? client.ElementAtP(indx) : null;
		}

		// Add a new Client
		public bool AddClient(IPEndPoint ep, bool async)
		{
			bool success = false;
			if (protocol == ProtocolType.Tcp)
			{
				if (async) clientAdd.AddP(ep);
				else
				{
					WaspClnt clnt = WaspClnt.Connect(ep);
					if (clnt != null)
					{
						success = true;
						client.AddP(clnt);
						if (fnClientAdded != null) fnClientAdded(this, new WaspEventArgs(WaspEvent.ClientAdded, ep));
					}
					else if (fnClientAddFailed != null) fnClientAddFailed(this, new WaspEventArgs(WaspEvent.ClientAddFailed, ep));
				}
			}
			return success;
		}

		// Remove a Client
		public bool RemoveClient(WaspClnt clnt, bool async)
		{
			bool success = false;
			if (protocol == ProtocolType.Tcp)
			{
				if (async) clientRmv.AddP(clnt);
				else
				{
					success = true;
					clnt.Close();
					client.RemoveP(clnt);
					if (fnClientRmvd != null) fnClientRmvd(this, new WaspEventArgs(WaspEvent.ClientRmvd, clnt));
				}
			}
			return success;
		}

		// Recieve Packet
		public WaspPkt RecievePacket()
		{
			return rcvPkt.PopP();
		}

		// Send Pakcet
		public bool SendPacket(WaspPkt pkt, bool async)
		{
			bool success = false;
			if (async) sndPkt.AddP(pkt);
			else
			{
				success = pkt.Send();
				if (success)
				{
					lock (sndPkt.Lock)
					{ sndPkts++; }
					if (fnPktSent != null) fnPktSent(this, new WaspEventArgs(WaspEvent.PktSent, pkt));
				}
				else if (fnPktSndFailed != null) fnPktSndFailed(this, new WaspEventArgs(WaspEvent.PktSndFailed, pkt));
			}
			return success;
		}

		// Add Event Fn
		public void SetEventFn(WaspEvent evnt, WaspEventFn fn)
		{
			switch (evnt)
			{
				case WaspEvent.ClientAcptd:
					fnClientAcptd = fn;
					break;
				case WaspEvent.ClientRjctd:
					fnClientRjctd = fn;
					break;
				case WaspEvent.ClientAdded:
					fnClientAdded = fn;
					break;
				case WaspEvent.ClientAddFailed:
					fnClientAddFailed = fn;
					break;
				case WaspEvent.ClientRmvd:
					fnClientRmvd = fn;
					break;
				case WaspEvent.PktRcvd:
					fnPktRcvd = fn;
					break;
				case WaspEvent.PktRcvFailed:
					fnPktRcvFailed = fn;
					break;
				case WaspEvent.PktSent:
					fnPktSent = fn;
					break;
				case WaspEvent.PktSndFailed:
					fnPktSndFailed = fn;
					break;
			}
		}

		// Remove Event Fn
		public void ClearEventFn(WaspEvent evnt)
		{
			switch (evnt)
			{
				case WaspEvent.ClientAcptd:
					fnClientAcptd = null;
					break;
				case WaspEvent.ClientRjctd:
					fnClientRjctd = null;
					break;
				case WaspEvent.ClientAdded:
					fnClientAdded = null;
					break;
				case WaspEvent.ClientAddFailed:
					fnClientAddFailed = null;
					break;
				case WaspEvent.ClientRmvd:
					fnClientRmvd = null;
					break;
				case WaspEvent.PktRcvd:
					fnPktRcvd = null;
					break;
				case WaspEvent.PktRcvFailed:
					fnPktRcvFailed = null;
					break;
				case WaspEvent.PktSent:
					fnPktSent = null;
					break;
				case WaspEvent.PktSndFailed:
					fnPktSndFailed = null;
					break;
			}
		}

		// CLose the Server
		public void Close()
		{
			thClientAcptr.Abort();
			thClientChkr.Abort();
			thClientAddr.Abort();
			thClientRmvr.Abort();
			thRcvPkt.Abort();
			thSndPkt.Abort();
			for (int i = 0; i < client.Count; i++)
			{
				client[i].Close();
				client[i] = null;
			}
			client = null;
			for (int i = 0; i < rcvPkt.Count; i++)
			{
				rcvPkt[i].Close();
				rcvPkt[i] = null;
			}
			for (int i = 0; i < rcvPkt.Count; i++)
			{
				sndPkt[i].Close();
				sndPkt[i] = null;
			}
			sndPkt = null;
			server.Close();
			server = null;
		}
	}
}
