﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace wAppSrvr.wasp
{
	class WaspList<T> : List<T>
	{
		public object Lock;
		public Semaphore Sem;
		public int CountP
		{
			get
			{
				lock (Lock)
				{ return Count; }
			}
		}

		// Create Parallel access list
		public WaspList(bool semUsed)
			: base()
		{
			Lock = new object();
			if(semUsed) Sem = new Semaphore(0, int.MaxValue);
		}
		public WaspList()
			: base()
		{
			Lock = new object();
		}


		// Get element
		public T ElementAtP(int indx)
		{
			lock (Lock)
			{
				if (indx < Count) return this[indx];
				return default(T);
			}
		}

		// Get next element
		public T NextElementP(ref int indx)
		{
			// wait until atleast one element is available
			if (Sem != null)
			{
				Sem.WaitOne();
				Sem.Release();
			}
			// get an element
			lock (Lock)
			{
				if (++indx >= Count) indx = 0;
				return (indx < Count) ? base[indx] : default(T);
			}
		}

		// Pop element
		public T PopP()
		{
			// wait for atleast one elemnt
			if (Sem != null) Sem.WaitOne();
			T elem = default(T);
			lock (Lock)
			{
				if (base.Count > 0)
				{
					elem = base[0];
					base.RemoveAt(0);
				}
			}
			return elem;
		}

		// Add element
		public void AddP(T elem)
		{
			lock(Lock)
			{ base.Add(elem); }
			if (Sem != null) Sem.Release();
		}

		// Remove element
		public bool RemoveP(T elem)
		{
			bool success = false;
			lock (Lock)
			{ success = base.Remove(elem); }
			if (success && Sem != null) Sem.WaitOne();
			return success;
		}
	}
}
