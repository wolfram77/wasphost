﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace wAppSrvr.wasp
{
	class WaspPkt
	{
		public WaspClnt Client;
		public byte[] Data;
		public const byte Begin = 0x7E;
		public const byte End = 0x81;
		
		// Create Packet
		public WaspPkt(WaspClnt clnt, byte[] dat)
		{
			Client = clnt;
			Data = dat;
		}

		// Send (Data)
		public bool Send()
		{
			return Client.SendData(Data);
		}

		// Close Packet: free memory
		public void Close()
		{
			Client = null;
			Data = null;
		}
	}
}
